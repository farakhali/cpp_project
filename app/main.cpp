#include "main_database.h"

using namespace emumba::training;

int main()
{
    database database;
    if (database.init_database())
    {
        database.poll_query();
    }
    return 0;
}
