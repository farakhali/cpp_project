#include <main_database.h>
#include "gtest/gtest.h"
#include <nlohmann/json.hpp>

using namespace std;
using namespace emumba::training;

database data_list;
accounts_data account;
personal_data person;

TEST(PersonalClass, Test_1)
{
    person.set_age(23);
    ASSERT_EQ(person.get_age(), 23);
}

TEST(AccountClass, Test_2)
{
    std::string name = "FirstName1 LastName1";
    account.set_name(name);
    ASSERT_EQ(account.get_name(), name);
}

TEST(FileExistorNOT, Test_3) // read/write if personal_data.json and query.txt file not exist
{
    ASSERT_EQ(data_list.init_database(), 1);
    ASSERT_EQ(data_list.read_file(), 1);
}

TEST(PersonalMapStruct, Test_4) // read from account_data map if account_id is present or not
{
    data_list.manage_personal_data("name", personal_data(23, "CityA", 23.45, 34.56));
    std::map<std::string, personal_data *>::iterator personal_iter = data_list.check_person_present("FirstName1 LastName1");
    EXPECT_EQ(personal_iter->second->get_age(), 30);
}

TEST(AccountMapStruct, Test_5) // read from account_data map if account_id is present or not
{
    data_list.manage_account_data("XYZ-423467", accounts_data("name", "bank", "currency", 2499.5));
    std::map<std::string, accounts_data *>::iterator account_iter = data_list.check_account_present("XYZ-423467");
    EXPECT_EQ(account_iter->second->get_bank(), "XYZ");
    // EXPECT_FALSE(data_list.check_account_present("XYZ-423464"));
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}