#include "personal_data.h"

using namespace emumba::training;
using namespace std;

personal_data::personal_data()
{
    set_age(0);
    set_city("city");
    set_cordinates(0.0, 0.0);
}
personal_data::personal_data(const uint8_t &age, const std::string &city, const float &latitude, const float &longitude)
{
    set_age(age);
    set_city(city);
    set_cordinates(latitude, longitude);
}

void personal_data::set_age(const uint8_t &age)
{
    _personal_data.age = age;
}
const uint8_t &personal_data::get_age() const
{
    return _personal_data.age;
}

void personal_data::set_city(const std::string &city)
{
    _personal_data.city = city;
}
const std::string &personal_data::get_city() const
{
    return _personal_data.city;
}

void personal_data::set_cordinates(const float &latitude, const float &longitude)
{
    _personal_data.cordinates.first = latitude;
    _personal_data.cordinates.second = longitude;
}

const std::pair<float, float> personal_data::get_cordinates() const
{
    return pair<float, float>{_personal_data.cordinates.first, _personal_data.cordinates.second};
}
