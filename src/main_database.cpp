#include "main_database.h"
#include <iostream>
#include <ctype.h>
#include <fstream>

using namespace emumba::training;
using namespace std;

void database::manage_personal_data(const std::string &name, const personal_data &data)
{
    personal_info_list.push_back(data);
    personal_info_map.insert(std::pair<std::string, personal_data *>(name, &personal_info_list.back()));
}
void database::manage_account_data(const std::string &account_id, const accounts_data &data_account)
{
    account_info_list.push_back(data_account);
    account_info_map.insert(std::pair<std::string, accounts_data *>(account_id, &account_info_list.back()));
}

std::map<std::string, personal_data *>::iterator database::check_person_present(const std::string &person_name)
{
    auto it = personal_info_map.find(person_name);
    if (it != personal_info_map.end())
    {
        return it;
    }
    else
    {
        return personal_info_map.end();
    }
}
std::map<std::string, accounts_data *>::iterator database::check_account_present(const std::string &account_id)
{
    auto it = account_info_map.find(account_id);
    if (it != account_info_map.end())
    {
        return it;
    }
    else
    {
        return account_info_map.end();
    }
}

json database::make_person_json_object(std::map<std::string, personal_data *>::iterator &personal_iter)
{
    json make_person_json;
    make_person_json = {{"Name", personal_iter->first},
                        {"age", personal_iter->second->get_age()},
                        {"city", personal_iter->second->get_city()},
                        {"coordinates", {{"lat", personal_iter->second->get_cordinates().first}, {"long", personal_iter->second->get_cordinates().second}}}};
    return make_person_json;
}
json database::make_account_json_object(std::map<std::string, accounts_data *>::iterator &account_iter)
{
    json make_account_json;
    make_account_json = {{"account id", account_iter->first},
                         {"name", account_iter->second->get_name()},
                         {"bank", account_iter->second->get_bank()},
                         {"balance", account_iter->second->get_balance()},
                         {"currency", account_iter->second->get_currency()}};
    return make_account_json;
}

bool database::init_database()
{
    std::fstream query_response("../query_response.json", std::ios::out);
    std::fstream personal_data_file("../personal_data.json");
    if (!personal_data_file.is_open())
    {
        json json_object = {"personal_data.json File not Found"};
        query_response << json_object.dump(4);
        return false;
    }
    try
    {
        json json_object = json::parse(personal_data_file);
        json_object = json_object["Account Holders Info"];
        for (auto i = json_object.begin(); i != json_object.end(); ++i)
        {
            manage_personal_data(i.value()["name"], personal_data(i.value()["age"], i.value()["city"], i.value()["coordinates"]["lat"], i.value()["coordinates"]["long"]));
            for (auto k = i.value()["accounts"].begin(); k != i.value()["accounts"].end(); ++k)
            {
                manage_account_data(k.value()["account id"], accounts_data(i.value()["name"], k.value()["bank"], k.value()["currency"], k.value()["balance"]));
            }
        }
        return true;
    }
    catch (const std::exception &e)
    {
        json json_object = {"personal_data.json File Empty"};
        query_response << json_object.dump(4);
        return false;
    }
    query_response.close();
}

bool database::read_file()
{
    std::string key;
    std::string value;
    std::fstream query_file("../query.txt");
    if (!query_file.is_open())
    {
        std::fstream query_response("../query_response.json", std::ios::out);
        json json_object = {"Query.txt File not Found"};
        query_response << json_object.dump(4);
        return false;
    }
    try
    {
        getline(query_file, key, ':');
        getline(query_file, value);
        key = key.substr(key.find('"') + 1, key.rfind('"') - 1);
        value = value.substr(value.find('"') + 1, value.rfind('"') - 2);
        query_value.key = key;
        query_value.value = value;
        return true;
    }
    catch (const std::exception &e)
    {
        std::fstream query_response("../query_response.json");
        json json_object = {"Unkown Error Occured Exit!!!"};
        query_response << json_object.dump(4);
        return false;
    }
}

bool database::check_number(std::string value)
{
    if (value.empty())
    {
        std::fstream query_response("../query_response.json", std::ios::out);
        json json_object = {"Empty Query"};
        query_response << json_object.dump(4);
        return false;
    }
    for (auto character : value)
    {
        if (!isdigit(character))
        {
            if (!(character == '.'))
            {
                std::fstream query_response("../query_response.json", std::ios::out);
                json json_object = {"Invalid Query"};
                query_response << json_object.dump(4);
                return false;
            }
        }
    }
    return true;
}

void database::poll_query()
{
    std::string previous_key = "NULL";
    std::string previous_value = "NULL";
    while (read_file())
    {
        if ((previous_value != query_value.value) || (previous_key != query_value.key))
        {
            bool flag = false;
            json object_to_dump;
            std::fstream query_response("../query_response.json", std::ios::out);
            if (((query_value.key == "Name") || (query_value.key == "name")))
            {
                auto it = personal_info_map.find(query_value.value);
                if (it != personal_info_map.end())
                {
                    object_to_dump.push_back(make_person_json_object(it));
                    query_response << object_to_dump.dump(4);
                }
                else
                {
                    json json_object = {"No Record Found against that Query"};
                    query_response << json_object.dump(4);
                }
            }
            else if ((query_value.key == "city" || query_value.key == "City"))
            {
                for (auto it = personal_info_map.begin(); it != personal_info_map.end(); ++it)
                {
                    if (it->second->get_city() == query_value.value)
                    {
                        flag = true;
                        object_to_dump.push_back(make_person_json_object(it));
                    }
                }
                if (flag)
                {
                    query_response << object_to_dump.dump(4);
                }
                else
                {
                    json json_object = {"No Record Found against that Query"};
                    query_response << json_object.dump(4);
                }
            }
            else if ((query_value.key == "age" || query_value.key == "Age"))
            {
                if (check_number(query_value.value))
                {
                    for (auto it = personal_info_map.begin(); it != personal_info_map.end(); ++it)
                    {
                        if (it->second->get_age() == std::stoi(query_value.value))
                        {
                            flag = true;
                            object_to_dump.push_back(make_person_json_object(it));
                        }
                    }
                    if (flag)
                    {
                        query_response << object_to_dump.dump(4);
                    }
                    else
                    {
                        json json_object = {"No Record Found against that Query"};
                        query_response << json_object.dump(4);
                    }
                }
            }
            else if ((query_value.key == "lat" || query_value.key == "Lat"))
            {
                if (check_number(query_value.value))
                {
                    for (auto it = personal_info_map.begin(); it != personal_info_map.end(); ++it)
                    {
                        if (it->second->get_cordinates().first == std::stof(query_value.value))
                        {
                            flag = true;
                            object_to_dump.push_back(make_person_json_object(it));
                        }
                        if (flag)
                        {
                            query_response << object_to_dump.dump(4);
                        }
                        else
                        {
                            json json_object = {"No Record Found against that Query"};
                            query_response << json_object.dump(4);
                        }
                    }
                }
            }
            else if ((query_value.key == "long" || query_value.key == "Long"))
            {
                if (check_number(query_value.value))
                {
                    for (auto it = personal_info_map.begin(); it != personal_info_map.end(); ++it)
                    {
                        if (it->second->get_cordinates().second == std::stof(query_value.value))
                        {
                            flag = true;
                            object_to_dump.push_back(make_person_json_object(it));
                        }
                        if (flag)
                        {
                            query_response << object_to_dump.dump(4);
                        }
                        else
                        {
                            json json_object = {"No Record Found against that Query"};
                            query_response << json_object.dump(4);
                        }
                    }
                }
            }
            else if ((query_value.key == "account id") || (query_value.key == "Account id"))
            {
                auto it = account_info_map.find(query_value.value);
                if (it != account_info_map.end())
                {
                    object_to_dump.push_back(make_account_json_object(it));
                    query_response << object_to_dump.dump(4);
                }
                else
                {
                    json json_object = {"No Record Found against that Query"};
                    query_response << json_object.dump(4);
                }
            }
            else if ((query_value.key == "bank") || (query_value.key == "Bank"))
            {
                for (auto it = account_info_map.begin(); it != account_info_map.end(); ++it)
                {
                    if (it->second->get_bank() == query_value.value)
                    {
                        object_to_dump.push_back(make_account_json_object(it));
                        flag = true;
                    }
                }
                if (flag)
                {

                    query_response << object_to_dump.dump(4);
                }
                else
                {
                    json json_object = {"No Record Found against that Query"};
                    query_response << json_object.dump(4);
                }
            }
            else if ((query_value.key == "currency") || (query_value.key == "Currency"))
            {
                for (auto it = account_info_map.begin(); it != account_info_map.end(); ++it)
                {
                    if (it->second->get_currency() == query_value.value)
                    {
                        object_to_dump.push_back(make_account_json_object(it));
                        flag = true;
                    }
                }
                if (flag)
                {

                    query_response << object_to_dump.dump(4);
                }
                else
                {
                    json json_object = {"No Record Found against that Query"};
                    query_response << json_object.dump(4);
                }
            }
            else if ((query_value.key == "balance") || (query_value.key == "Balance"))
            {
                if (check_number(query_value.value))
                {
                    for (auto it = account_info_map.begin(); it != account_info_map.end(); ++it)
                    {
                        if (it->second->get_balance() == std::stof(query_value.value))
                        {
                            flag = true;
                            object_to_dump.push_back(make_account_json_object(it));
                        }
                    }
                    if (flag)
                    {
                        query_response << object_to_dump.dump(4);
                    }
                    else
                    {
                        std::fstream query_response("../query_response.json");
                        json json_object = {"No Record Found against that Query"};
                        query_response << json_object.dump(4);
                    }
                }
            }
            else if ((query_value.key == "clear" || query_value.key == "Clear") && (query_value.value == "file" || query_value.value == "File"))
            {
                std::fstream query_response("../query_response.json", std::ios::out);
            }
            else
            {
                object_to_dump = {"No Record Found aganist that Query"};
                query_response << object_to_dump.dump(4);
            }
            previous_key = query_value.key;
            previous_value = query_value.value;
        }
        sleep(1);
    }
}
