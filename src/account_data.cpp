#include "account_data.h"

using namespace emumba::training;
using namespace std;

accounts_data::accounts_data()
{
    set_name("name");
    set_currency("null");
    set_bank("null");
    set_balance(0);
}
accounts_data::accounts_data(const std::string &name, const std::string &bank, const std::string &currency, const float &balance)
{
    set_name(name);
    set_currency(currency);
    set_bank(bank);
    set_balance(balance);
}
void accounts_data::set_name(const std::string &name)
{
    account_data.name = name;
}
const std::string &accounts_data::get_name() const
{
    return account_data.name;
}
void accounts_data::set_bank(const std::string &bank)
{
    account_data.bank = bank;
}
const std::string &accounts_data::get_bank() const
{
    return account_data.bank;
}

void accounts_data::set_currency(const std::string &currency)
{
    account_data.currency = currency;
}
const std::string &accounts_data::get_currency() const
{
    return account_data.currency;
}

void accounts_data::set_balance(const float &balance)
{
    account_data.balance = balance;
}
const float &accounts_data::get_balance() const
{
    return account_data.balance;
}