#ifndef _ACCOUNT_DATA_H
#define _ACCOUNT_DATA_H

#include <string>

namespace emumba::training
{
    class accounts_data
    {
    private:
        struct account_data_sruct
        {
            std::string name;
            std::string bank;
            std::string currency;
            float balance;
        } account_data;

    public:
        accounts_data();
        accounts_data(const std::string &name, const std::string &bank, const std::string &currency, const float &balance);

        void set_name(const std::string &name);
        const std::string &get_name() const;

        void set_bank(const std::string &bank);
        const std::string &get_bank() const;

        void set_currency(const std::string &currency);
        const std::string &get_currency() const;

        void set_balance(const float &balance);
        const float &get_balance() const;
    };
}

#endif
