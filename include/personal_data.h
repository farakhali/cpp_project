#ifndef _PERSONAL_DATA_H
#define _PERSONAL_DATA_H

#include <string>

namespace emumba::training
{
    class personal_data
    {
    private:
        struct personal_data_struct
        {
            uint8_t age;
            std::string city;
            std::pair<float, float> cordinates;
        } _personal_data;

    public:
        personal_data();
        personal_data(const uint8_t &age, const std::string &city, const float &latitude, const float &longitude);

        void set_age(const uint8_t &age);
        const uint8_t &get_age() const;

        void set_city(const std::string &city);
        const std::string &get_city() const;

        void set_cordinates(const float &latitude, const float &longitude);
        const std::pair<float, float> get_cordinates() const;
    };
}

#endif
