#ifndef _DATABASE_H
#define _DATABASE_H

#include <map>
#include <list>
#include <fstream>
#include <string>
#include <iterator>
#include <unistd.h>
#include "personal_data.h"
#include "account_data.h"
#include <nlohmann/json.hpp>

using json = nlohmann::json;
namespace emumba::training
{
    class database
    {
    private:
        std::map<std::string /* Account Holder Name */, personal_data * /* Pointer to Personal Data Struct */> personal_info_map;
        std::list<personal_data> personal_info_list;
        std::map<std::string /*Account ID/No. */, accounts_data * /* Pointer to Account Data Struct */> account_info_map;
        std::list<accounts_data> account_info_list;
        struct query
        {
            std::string key;
            std::string value;
        } query_value;

    public:
        void manage_personal_data(const std::string &name, const personal_data &data);
        void manage_account_data(const std::string &account_id, const accounts_data &data_account);

        std::map<std::string, personal_data *>::iterator check_person_present(const std::string &person_name);
        std::map<std::string, accounts_data *>::iterator check_account_present(const std::string &account_id);

        json make_person_json_object(std::map<std::string, personal_data *>::iterator &personal_iter);
        json make_account_json_object(std::map<std::string, accounts_data *>::iterator &account_iter);

        bool init_database();
        bool read_file();
        void poll_query();

        bool check_number(std::string value);
    };
}

#endif