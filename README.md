# CPP Project

This repo include fall 2021 C++ training project.

## Directory Structure

The directory structure of the app is  

```bash  

cpp_project (main directory)
│   README.md
│   CMakeLists.txt
│   personal_data.json
│   query.txt
│   Query_response.json
|   .gitignore
│   .gitmodules
|
└───include
│   └───personal_data.h
│   └───account_data.h
│   └───main_database.h
|
└───src
│   └───personal_data.cpp
│   └───account_data.cpp
│   └───main_database.cpp
|   └───CMakeLists.txt
|
└───app
|   └───main.cpp
|   └───CMakeLists.txt
|
└───benchmarking
│   └───benchmark.cpp
│   └───CMakeLists.txt
│   └───googletest_library
│   └───benchmark_library
|
└───unittesting
│   └───unit_test.cpp
│   └───CMakeLists.txt
|
└───json_library
    └───nlohmann_json_library

```

## About this Application

The main task in this project is to make a database-querying application, that will perform following operations:  

- Read and store the information of some bank account holders from a json file  
- Continuously poll a text file and read query commands from there  
- Execute the command by writing the requested information to another json file

## How to Run App

---

```bash
git clone https://gitlab.com/farakhali/cpp_project.git  
cd cpp_project 
git submodule update --init
mkdir build  
cd build
cmake -DDEBUG=ON/OFF ..  
make
```

The final executabale will be `./project`  
To run unit test Executable `./project_unittest`  
To run Bench Mark Executable `./project_benchmark`

---

## How to Search in Database

The search mechanism in the database based on the key : value pair structure. To serach anyone's record in database Run the main executable and got to `query.txt` file and follow the `"Key" : "Value"` pair formate to search. for example, if you want search by name then type the following command  `"name" : "FirstName LastName"` and save the `query.txt` file. Now go to `query_response.json` file and view query response. Simillar formate goes for other fields like `City, Bank, Account ID, Lat(latitude), Long(Longitude), Currency, Balance`. To Clear the `query_response.txt` file content type `"clear" : "file"`
