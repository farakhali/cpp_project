#include <main_database.h>
#include "benchmark/benchmark.h"
#include <nlohmann/json.hpp>
#include <map>

using namespace std;
using namespace emumba::training;
using json = nlohmann::json;

database data_list;

static void BM_InitializeData(benchmark::State &state)
{
    for (auto _ : state)
    {
        data_list.init_database();
    }
}

static void BM_PersonBenchMark(benchmark::State &state)
{
    for (auto _ : state)
    {
        data_list.manage_personal_data("name", personal_data(23, "CityA", 23.45, 34.56));
        std::map<std::string, personal_data *>::iterator personal_iter = data_list.check_person_present("name");
        json make_person_json;
        make_person_json = data_list.make_person_json_object(personal_iter);
        data_list.dump_json_object(make_person_json);
    }
}

static void BM_AccountBenchMark(benchmark::State &state)
{
    for (auto _ : state)
    {
        data_list.manage_account_data("RE4-426351", accounts_data("name", "bank", "currency", 2499.5));
        std::map<std::string, accounts_data *>::iterator account_iter = data_list.check_account_present("RE4-426351");
        json make_account_json;
        make_account_json = data_list.make_account_json_object(account_iter);
        data_list.dump_json_object(make_account_json);
    }
}

/*static void BM_PersonBenchmark(benchmark::State &state)
{
    for (auto _ : state)
    {
        data_list.manage_personal_data("name", personal_data(23, "CityA", 23.45, 34.56));
        personal_data *personal_iter;
        personal_iter = data_list.check_person_present("Name");
        json obj = data_list.make_person_json_object(personal_iter);
        data_list.dump_json_object(obj);
    }
}*/

static void BM_InData(benchmark::State &state)
{
    for (auto _ : state)
        data_list.read_file();
}

BENCHMARK(BM_InitializeData)->DenseRange(1, 1000, 100);
BENCHMARK(BM_PersonBenchMark)->Range(1, 10000);
BENCHMARK(BM_AccountBenchMark)->Range(1, 10000);
BENCHMARK(BM_InData)->Range(1, 10000);
BENCHMARK_MAIN();
